﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiguienteNivel : MonoBehaviour
{
    public string nivelACargar;
    public float retraso;
    // Start is called before the first frame update
    [ContextMenu("Activar carga")]
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Puntos.puntos != 10)
        {
            Invoke("CargarNivel",retraso);
            Application.LoadLevel(nivelACargar);
        }
        
    }
    
    public bool EsUltimoNivel()
    {
        return (nivelACargar.Equals("portada"));
    }
}
