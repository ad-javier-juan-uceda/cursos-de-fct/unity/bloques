﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Saltarinteractivo : MonoBehaviour , IPointerClickHandler
{
    // Start is called before the first frame update
    public Rigidbody rig;
    public float velocidadInicial = 600f;
    public Transform barra;
    
    public void OnPointerClick(PointerEventData eventData)
    {
        transform.SetParent(null);
        //rig.isKinematic = false;
        rig.AddForce(new Vector3(velocidadInicial, velocidadInicial, 0));
    }
  
    void Start()
    {
       
    }

    private void Awake()
    {
        barra = GetComponentInParent<Transform>();
    }
}
