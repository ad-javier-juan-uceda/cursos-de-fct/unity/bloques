﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Vidas : MonoBehaviour
{
    public Text textoVidas;
    public static int vidas = 3;
    public GameObject GameObject;
    
    // Start is called before the first frame update
    void Start()
    {
        textoVidas.text = "Vidas :" + Vidas.vidas;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void PerderVida()
    {
        textoVidas.text = "Vidas :" + Vidas.vidas--;
        if (vidas <= 0)
        {
            vidas = 3;
            Puntos.puntos = 0;
            GameObject.SetActive(true);
            Thread.Sleep(3000);
            Application.LoadLevel("SampleScene 1");
            GameObject.SetActive(false);
        }
    }
}
