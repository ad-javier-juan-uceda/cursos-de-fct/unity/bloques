﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour
{
    public Rigidbody rig;
    public float velocidadInicial = 600f;
	public Vector3 positionInicial;
    private bool enJuego;
    public AudioSource rebote;
    

    //public Vector3 posicionInicial;

    public Transform barra;
    // Start is called before the first frame update
    void Start()
    {
        positionInicial = transform.position;
    }
    void Awake()
    {
        barra = GetComponentInParent<Transform>();
    }
    public void Reset()
    {
        rig.position = positionInicial;
        //transform.position = positionInicial;
        transform.SetParent(barra);
        
        enJuego = false;
        rig.isKinematic = false;
        rig.velocity = Vector3.zero;
    }
    // Update is called once per frame
    public void Update()
    {
        if (!enJuego && Input.GetButtonDown("Fire1"))
        {
            //rebote.Play();
            enJuego = false;
            transform.SetParent(null);
            rig.isKinematic = false;
            rig.AddForce(new Vector3(velocidadInicial, velocidadInicial, 0));
        }
    }
    
    public void Update2()
    {
        if (!enJuego)
        {
            rebote.Play();
            enJuego = false;
            transform.SetParent(null);
            rig.isKinematic = false;
            rig.AddForce(new Vector3(velocidadInicial, velocidadInicial, 0));
        }
    }
}
