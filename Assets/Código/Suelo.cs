﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suelo : MonoBehaviour
{
    public Vidas vida;
    public Pelota pelota;

    public Script1 bloque;

    // Start is called before the first frame update
    void Start()
    {
    }

    void OnTriggerEnter()
    {
        vida.PerderVida();
        pelota.Reset();
        bloque.Reset();
    }

    // Update is called once per frame
    void Update()
    {
    }
}