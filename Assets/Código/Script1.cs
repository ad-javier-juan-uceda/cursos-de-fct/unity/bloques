﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script1 : MonoBehaviour
{

    public float velocidad = 20f;

    private Vector3 positionInicial;
    // Start is called before the first frame update
    void Start()
    {
        positionInicial = transform.position;
    }

    public void Reset()
    {
        transform.position = positionInicial;
    }

    
    // Update is called once per frame
    void Update()
    {
        float tecladohorizontal = Input.GetAxisRaw("Horizontal");
        float positionX = transform.position.x + (tecladohorizontal * velocidad * Time.deltaTime);
        transform.position = new Vector3(Mathf.Clamp(positionX,-27,-27), transform.position.y, transform.position.z);
    }
}
