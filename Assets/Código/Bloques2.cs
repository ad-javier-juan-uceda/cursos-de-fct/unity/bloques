﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloques2 : MonoBehaviour
{
    public GameObject efectoParticulas;
    //public string nivelACargar;
    //public float retraso;
    public Puntos puntos;
    // Start is called before the first frame update
    void OnCollisionEnter()
    {
        Instantiate(efectoParticulas,transform.position,Quaternion.identity);
        puntos.PerderVida();
        Destroy(gameObject);
    }

    // Update is called once per frame
    void OnTriggerEnter()
    {
        
    }
}
