﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Puntos : MonoBehaviour
{
    public Text textoPuntos;
    public static int puntos = 0;
    public bool salir;
    public AudioSource punto;
    public AudioSource completado;
    // Start is called before the first frame update
    void Start()
    {
        textoPuntos.text = "Puntos :" + Puntos.puntos;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (salir)
            {
                Application.Quit();
            }
            else
            {
                Application.LoadLevel("SampleScene 1");
            }
           
        }
    }
    
    public void PerderVida()
    {
        textoPuntos.text = "Puntos :" + Puntos.puntos++;
        //punto.Play();
        if (puntos >= 10)
        {
            puntos = 0;
            textoPuntos.text = "Puntos :" + Puntos.puntos;
            Invoke("CargarNivel",1000);
            //completado.Play();
            Application.LoadLevel("SampleScene 2");
        }

    }
}
